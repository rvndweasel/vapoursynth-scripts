from vsutil import *
import vapoursynth as vs
import havsfunc as haf
import kagefunc as kgf
import mvsfunc as mvf
import fvsfunc as fvf
import lvsfunc as lvf
import EoEfunc as eoe
import nnedi3_rpow2
from vsTAAmbk import TAAmbk as taa
import vsutil
import sys

core = vs.core

DEFAULT_THREADS=24
DEFAULT_CACHE=28

DEFAULT_PATH = "Horimiya - 01.mkv"

#region setup

print(file=sys.stderr)

try:
    core.max_cache_size = int(max_cache_size.decode("utf-8")) * 1024
    print(f"Vapoursynth: INFO --> Allocated max {core.max_cache_size/1024}GiB of RAM", file=sys.stderr)
except ValueError:
    print(f"Vapoursynth: ERROR --> That's not a number. Allocating max {DEFAULT_CACHE}GiB of RAM", file=sys.stderr)
    core.max_cache_size = DEFAULT_CACHE * 1024
except:
    print(f"Vapoursynth: WARNING --> No max cache size set or error reading input, allocating max {DEFAULT_CACHE}GiB of RAM", file=sys.stderr)
    core.max_cache_size = DEFAULT_CACHE * 1024

try:
    core.num_threads = int(num_threads.decode("utf-8"))
    print(f"Vapoursynth: INFO --> Using {core.num_threads} threads", file=sys.stderr)
except ValueError:
    print(f"Vapoursynth: ERROR --> That's not a number. Using {DEFAULT_THREADS} threads", file=sys.stderr)
    core.num_threads = DEFAULT_THREADS
except:
    print(f"Vapoursynth: WARNING --> No thread count set or error reading input, using {DEFAULT_THREADS} threads by default", file=sys.stderr)
    core.num_threads = DEFAULT_THREADS

try:
    GPU = int(GPU.decode("utf-8"))
    print(f"Vapoursynth: INFO --> Using GPU {GPU}", file=sys.stderr)
except ValueError:
    print(f"Vapoursynth: ERROR --> That's not a number. Using GPU 0", file=sys.stderr)
    GPU = 0
except:
    print("Vapoursynth: WARNING --> No GPU specified or error reading input, using GPU 0 by default", file=sys.stderr)
    GPU = 0

try:
    src_path = src_path.decode("utf-8")
    debug = False
except:
    src_path = DEFAULT_PATH
    print(f"Vapoursynth: WARNING --> No input video specified, using default {src_path}", file=sys.stderr)
    debug = True

def rescale(source: vs.VideoNode, width: int = None, height: int = 0, kernel: str = 'bilinear', taps: int = 4,
                  b: float = 1 / 3, c: float = 1 / 3, mask_detail: bool = False, rescale_threshold: float = 0.015,
                  rescale_mask_zones: str = '', denoise: bool = False, bm3d_sigma: float = 1, radius1: int = 0,
                  profile1: str = "fast", knl_luma: float = 0.6, use_gpu: bool = False, get_mask: bool = False) \
        -> vs.VideoNode:
    if not height:
        raise ValueError('inverse_scale: you need to specify a value for the output height')

    from vsutil import iterate, split, join, get_y
    import kagefunc as kgf
    import mvsfunc as mvf
    import fvsfunc as fvf
    import nnedi3_rpow2

    def get_descale_mask(source: vs.VideoNode, upscaled: vs.VideoNode, threshold: float = 0.015) \
        -> vs.VideoNode:
        mask = core.std.Expr([source, upscaled], f"x y - abs {threshold} < 0 1 ?")
        mask = iterate(mask, core.std.Maximum, 2)
        mask = iterate(mask, core.std.Inflate, 2)
        return mask

    only_luma = source.format.num_planes == 1

    if get_depth(source) != 32:
        src32 = core.resize.Point(source, format=source.format.replace(bits_per_sample=32, sample_type=vs.FLOAT))

    planes = split(src32)
    planes[0] = descaled = kgf._descale_luma(planes[0], width, height, kernel, taps, b, c)

    if denoise:
        if use_gpu:
            planes[0] = core.knlm.KNLMeansCL(planes[0], a=2, h=knl_luma, d=3)
        else:
            import mvsfunc as mvf
            planes[0] = mvf.BM3D(planes[0], radius1=radius1, sigma=bm3d_sigma, profile1=profile1)

    # denoise
    planes[0] = core.resize.Point(planes[0], format=vs.GRAY16)
    planes[0] = mvf.BM3D(planes[0], pre=core.bilateral.Gaussian(planes[0], sigma=0.6), sigma=1.45, radius1=1, profile1="lc")
    planes[0] = core.resize.Point(planes[0], format=vs.GRAYS)

    planes[0] = haf.FineDehalo(planes[0], rx=2.0, ry=2.0, darkstr=0.2, brightstr=0.8)

    planes[0] = nnedi3_rpow2.nnedi3_rpow2(planes[0], rfactor=2).resize.Spline36(source.width, source.height)

    if only_luma and not mask_detail:
        return planes[0]

    if mask_detail:
        upscaled = fvf.Resize(descaled, source.width, source.height, kernel=kernel, taps=taps, a1=b, a2=c)
        mask = get_descale_mask(get_y(src32), upscaled, rescale_threshold)
        planes[0] = core.std.MaskedMerge(planes[0], get_y(src32), mask)
        mask = core.resize.Point(mask, format=source.format.replace(color_family=vs.GRAY, subsampling_h=0, subsampling_w=0))
    scaled = join(planes)
    scaled = core.resize.Point(scaled, format=source.format)
    return (scaled, mask) if get_mask else scaled

#endregion

print(f"Vapoursynth: INFO --> Beginning job - (", src_path, ")", file=sys.stderr)
src = core.lsmas.LWLibavSource(src_path)
src = core.resize.Point(src, format=vs.YUV420P16)

#--------- Reference Frames --------#
'''
    11643
    13551
    16016
'''

#------------ Prefilter ------------#
prefilter = core.dfttest.DFTTest(src, tbsize=1, sigma=2)
smde = haf.SMDegrain(src, tr=3, thSAD=125, RefineMotion=True, prefilter=prefilter)


#------------ Rescaling ------------#
rescaled = rescale(smde, width=1552, height=873, kernel='bicubic', b=0, c=3/4, mask_detail=True, rescale_threshold=0.04)


rescaled = core.resize.Point(rescaled, format=vs.YUV420P16)

#------------ Denoising ------------#

# split planes in YUV
planes = split(rescaled)

# denoise
planes[1] = core.knlm.KNLMeansCL(planes[1], d=2, a=3, s=6, h=0.6, device_id=GPU)
planes[2] = core.knlm.KNLMeansCL(planes[2], d=2, a=3, s=6, h=0.6, device_id=GPU)

# join seperated YUV planes
denoise = join(planes)


#---------- AntiAliasing -----------#
aa = taa(denoise, 3)


#---------- Dehalo/Deband ----------#
chromaFix = core.warp.AWarpSharp2(aa, thresh=128, blur=1, depth=6, planes=[1,2], type=1)


dehalo = haf.FineDehalo(chromaFix, darkstr=0.1, brightstr=0.8)
deband = core.f3kdb.Deband(dehalo, range=15, y=32, cb=28, cr=28, grainy=16, grainc=7)


#------------ Add Grain ------------#
addGrain = kgf.adaptive_grain(deband, 0.35, luma_scaling=10)


final = addGrain
final = core.resize.Point(final, format=vs.YUV420P10)
final.set_output(0)

if debug:
    try:
        from EoEfunc import debug_output
    except:
        print("Vapoursynth: WARNING --> Unable to output debug clips. Missing EoEfunc")
    debug_output(src, "src")
    debug_output(smde, "smde")
    debug_output(rescaled, "rescaled")
    debug_output(denoise, "denoise")
    debug_output(dehalo, "dehalo")
    debug_output(deband, "deband")
    debug_output(addGrain, "grain")
