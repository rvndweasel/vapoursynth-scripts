# Encoding Scripts
This repository is meant to be a central public location of storing my vapoursynth scripts for movies and tv shows. I give permission to use my code in any (legal) way that meets ones needs. Do understand though that due to the constant evolving nature of vapoursynth, expect some tinkering to make my code work for your needs.
