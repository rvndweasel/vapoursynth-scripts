from vsutil import *
import vapoursynth as vs
import sys

import havsfunc as haf
import kagefunc as kgf
import mvsfunc as mvf
import fvsfunc as fvf
import lvsfunc as lvf

import nnedi3_rpow2
from vsTAAmbk import TAAmbk as taa

import EoEfunc as eoe


# Region Setup -------------------------
# Import File
DEFAULT_PATH = "Fairy Tail S08E01 2018 1080p.mkv"

# Hardware Parameters
DEFAULT_THREADS=8
DEFAULT_CACHE=40

env = eoe.setup_env(globals())
core: vs._CoreProxy = env['core']
src_path: str       = env['src_path']
GPU: int            = env['GPU']
debug: bool         = env['debug']


def rescale(source: vs.VideoNode, width: int = None, height: int = 0, kernel: str = 'bilinear', taps: int = 4,
                  b: float = 1 / 3, c: float = 1 / 3, mask_detail: bool = False, rescale_threshold: float = 0.015,
                  rescale_mask_zones: str = '', denoise: bool = False, bm3d_sigma: float = 1, radius1: int = 0,
                  profile1: str = "fast", knl_luma: float = 0.6, use_gpu: bool = False, get_mask: bool = False) \
        -> vs.VideoNode:
    if not height:
        raise ValueError('inverse_scale: you need to specify a value for the output height')

    from vsutil import iterate, split, join, get_y
    import kagefunc as kgf
    import fvsfunc as fvf
    import nnedi3_rpow2

    def get_descale_mask(source: vs.VideoNode, upscaled: vs.VideoNode, threshold: float = 0.015) \
        -> vs.VideoNode:
        mask = core.std.Expr([source, upscaled], f"x y - abs {threshold} < 0 1 ?")
        mask = iterate(mask, core.std.Maximum, 2)
        mask = iterate(mask, core.std.Inflate, 2)
        return mask

    only_luma = source.format.num_planes == 1

    if get_depth(source) != 32:
        src32 = core.resize.Point(source, format=source.format.replace(bits_per_sample=32, sample_type=vs.FLOAT))

    planes = split(src32)
    planes[0] = descaled = kgf._descale_luma(planes[0], width, height, kernel, taps, b, c)

    if denoise:
        if use_gpu:
            planes[0] = core.knlm.KNLMeansCL(planes[0], a=2, h=knl_luma, d=3)
        else:
            import mvsfunc as mvf
            planes[0] = mvf.BM3D(planes[0], radius1=radius1, sigma=bm3d_sigma, profile1=profile1)

    planes[0] = nnedi3_rpow2.nnedi3_rpow2(planes[0], rfactor=2).resize.Spline64(source.width, source.height)

    if only_luma and not mask_detail:
        return planes[0]

    if mask_detail:
        upscaled = fvf.Resize(descaled, source.width, source.height, kernel=kernel, taps=taps, a1=b, a2=c)
        mask = get_descale_mask(get_y(src32), upscaled, rescale_threshold)
        planes[0] = core.std.MaskedMerge(planes[0], get_y(src32), mask)
        mask = core.resize.Point(mask, format=source.format.replace(color_family=vs.GRAY, subsampling_h=0, subsampling_w=0))
    scaled = join(planes)
    scaled = core.resize.Point(scaled, format=source.format)
    return (scaled, mask) if get_mask else scaled


def cleanEdges(szClip, bSimple=True, szStrength="light", szMask='default', iBinarizeThresh=5000, bWarpChroma=True):
    if(bSimple == True):

        # Make sure clip is in correct format
        szClip = core.resize.Point(szClip, format=vs.YUV420P16)

        # Initialize strengths with corresponding values
        dStrengths   = {"very light":0, "light":1, "medium":2, "heavy":3, "very heavy":4}
        lBiSigmaR    = [0.001, 0.003, 0.005, 0.007, 0.01]
        lWarpBlur    = [1, 1, 2, 2, 3]
        lWarpDepth   = [2, 3, 4, 6, 8]

        # Make sure specified strength exists
        if szStrength in dStrengths:
            # Get Index for all values of specified strength
            iIndex = dStrengths.get(szStrength)

            # Assign all values located at iIndex
            fSigmaR = lBiSigmaR[iIndex]
            iBlur   = lWarpBlur[iIndex]
            iDepth  = lWarpDepth[iIndex]

        # Perform the "cleaning"
        szClean = haf.EdgeCleaner(szClip)
        szCleanEdges = core.bilateral.Bilateral(szClean, sigmaR=fSigmaR)

        # ContraSharpening
        szPlanes = split(szCleanEdges)
        szPlanes[0] = eoe.ContraSharpening(plane(szCleanEdges, 0), plane(szClean, 0))
        szCleanEdges = join(szPlanes)

        # Make the chroma "stick" towards the edges
        if(bWarpChroma == True):
            szCleanEdges = core.warp.AWarpSharp2(szCleanEdges, blur=iBlur, type=1, depth=iDepth, planes=[1,2])


    if (bSimple == False):
        # Need to do
            # Look into mask to see about max/min

        # Make sure clip is in correct formatszClip = core.resize.Point(szClip, format=vs.YUV420P16)


        # Initialize strengths with corresponding values
        dStrengths      = {"very light":0, "light":1, "medium":2, "heavy":3, "very heavy":4}
        lBiSigmaR       = [0.003, 0.005, 0.01, 0.02, 0.03]
        lWarpBlur       = [1, 1, 2, 2, 3]
        lWarpDepth      = [2, 3, 5, 8, 16]
        lWaveletThresh  = [1.1, 1.25, 1.5, 1.75, 2.0]

        # Make sure specified strength exists
        if szStrength in dStrengths:
            # Get Index for all values of specified strength
            iIndex = dStrengths.get(szStrength)

            # Assign all values located at iIndex
            fSigmaR = lBiSigmaR[iIndex]
            iBlur   = lWarpBlur[iIndex]
            iDepth  = lWarpDepth[iIndex]
            fThresh = lWaveletThresh[iIndex]

            # Set Edgemask
            szReferenceMask = szMask

        else:
            print("Error -> Specified strength not found")


        # Apply Some High/Low Thresholding With Wavelet Denoising
        szDenoiseW = core.vd.VagueDenoiser(szClip, method=0, threshold=fThresh)
        szDenoiseW = core.vd.VagueDenoiser(szDenoiseW, method=1, nsteps=10, threshold=fThresh)

        szPlanes = split(szDenoiseW)
        szPlanes[0] = eoe.ContraSharpening(szPlanes[0], plane(szClip, 0))
        szClean = join(szPlanes)

        # Clean Up The Chroma
        if(bWarpChroma == True):
            szClean = core.warp.AWarpSharp2(szClean, blur=iBlur, depth=iDepth, planes=[1,2])


        # Create Edgemask
        szPlanes = split(szClean)

        # Initialize Edgemask
        if(szReferenceMask == "default"):
            szReferenceMask = kgf.kirsch(szPlanes[0])

        elif(szReferenceMask == "retinex"):
            szReferenceMask = kgf.retinex_edgemask(szPlanes[0])

        elif(szReferenceMask == "sobel"):
            szReferenceMask = core.std.Sobel(szPlanes[0])

        # Adjust Edgemask
        szMaskOuter = iterate(szReferenceMask, core.std.Maximum, 5)
        szMaskInner = iterate(szMaskOuter, core.std.Minimum, 5)
        szMaskFinal = core.std.Expr([szMaskOuter, szMaskInner], 'x y -').std.Binarize(8500).std.Deflate()

        szReferenceMask = join(szPlanes)


        # Perform the final operations
        szCleanEdges = core.bilateral.Bilateral(szClean, sigmaR=fSigmaR)
        szCleanEdges = core.rgvs.Repair(szCleanEdges, szClean, mode=1)
        szCleanEdges = core.std.MaskedMerge(szClean, szCleanEdges, szMaskFinal)

        # ContraSharpening
        szPlanes = split(szCleanEdges)
        szPlanes[0] = eoe.ContraSharpening(szPlanes[0], plane(szClean, 0))
        szCleanEdges = join(szPlanes)

    return szCleanEdges


#--------- Reference Frames ---------#
"""
24707
25633
27604
29727
"""


#------------- Source --------------#
src = core.lsmas.LWLibavSource(src_path)
src = core.resize.Point(src, format=vs.YUV420P16)

try:
    src = core.edgefixer.Continuity(src,1,1,1,1,2)
except:
    src = core.edgefixer.ContinuityFixer(src,1,1,1,1,2)

#------------ PREFILTER ------------#
# Basic prefilter
prefilter = eoe.CMDegrain(src, tr=3, thSAD=256, RefineMotion=5, contrasharp=True, prefilter=3)


#------------ Rescaled -------------#
rescaled = rescale(prefilter, width=1280, height=720, kernel='bicubic', b=1/3, c=1/3, mask_detail=True, rescale_threshold=0.04)
rescaled = core.resize.Point(rescaled, format=vs.YUV420P16)


#------------ Denoising ------------#
# split planes in YUV
planes = split(rescaled)

# denoise
planes[0] = mvf.BM3D(planes[0], pre=core.bilateral.Gaussian(planes[0], sigma=1.0), sigma=2, radius1=1, profile1="lc")
planes[1] = core.knlm.KNLMeansCL(planes[1], d=2, a=3, s=6, h=1.1, device_id=GPU)
planes[2] = core.knlm.KNLMeansCL(planes[2], d=2, a=3, s=6, h=1.1, device_id=GPU)

# Sharpen the luma plane while its already split
planes[0] = eoe.ContraSharpening(planes[0], plane(rescaled, 0))

# join seperated YUV planes
denoise = join(planes)


#---------- AntiAliasing -----------#
aa = taa(denoise, 3)


#------------ DeHaloing ------------#
dehalo = haf.FineDehalo(aa, darkstr=0.2, brightstr=0.8)


#------------ DeBanding ------------#
deband = core.f3kdb.Deband(dehalo, range=15, y=36, cb=30, cr=30, grainy=16, grainc=9)


#------------ Add Grain ------------#
addGrain = kgf.adaptive_grain(deband, 0.335, luma_scaling=10)


final = addGrain
final = core.resize.Point(final, format=vs.YUV420P10)
final.set_output(0)

if debug:
    try:
        from EoEfunc import debug_output
    except:
        print("Vapoursynth: WARNING --> Unable to output debug clips. Missing EoEfunc")
        exit

    #debug_output(src, "src")
    #debug_output(prefilter, "prefilter")
    #debug_output(rescaled, "1313")
    #debug_output(denoise, "denoise")
    #debug_output(aa, "aa")
    #debug_output(dehalo, "dehalo")
    #debug_output(deband, "deband")
    #debug_output(addGrain, "grain")
